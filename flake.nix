{
  description = "gluCratesIO";

  outputs = { self }: {
    strok = {
      spici = "lamdy";
    };

    datom = { mkCargoNix }:

    { nightly ? true }:
    let
      cargoNix = mkCargoNix {
        cargoNixPath = ./Cargo.nix;
        inherit nightly;
      };

      mkCrate = packageId: packageDatom:
      cargoNix.internal.buildRustCrateWithFeatures {
        inherit packageId;
      };

    in
    builtins.mapAttrs mkCrate cargoNix.internal.crates;
  };
}
